use std::io::stdin;

fn main() {
    let mut input = String::new();
    stdin().read_line(&mut input).unwrap();
    let t:Vec<i32> = input.trim().split_whitespace().map(|x| {x.parse().unwrap()}).collect();
    let a = t[0];
    let b = t[1];
    let v = t[2];
    // let mut current = 0;
    // let mut counter = 1;
    // loop {
    //     current += a;
    //     if current >= v {
    //         println!("{}", counter);
    //         break;
    //     }
    //     current -= b;
    //     counter += 1;
    // }
    let d = (((v-b) as f64)/((a-b) as f64)).ceil() as i32;
    println!("{}", d)
}

#[test]
fn test_2869() {
    use assert_cmd::Command;

    let mut cmd = Command::cargo_bin("p2869").unwrap();
    cmd.write_stdin("2 1 5\n").assert().stdout("4\n");
    cmd.write_stdin("5 1 6\n").assert().stdout("2\n");
    cmd.write_stdin("100 99 1000000000\n").assert().stdout("999999901\n");
}