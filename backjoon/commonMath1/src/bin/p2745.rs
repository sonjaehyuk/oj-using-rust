use std::io::stdin;

fn convert_dec(c: char) -> u8 {
    match c {
        '0'|'1'|'2'|'3'|'4'|'5'|'6'|'7'|'8'|'9' => c as u8 - 48,
        _ => c as u8 - 55
    }

}

fn main() {
    let mut input = String::new();
    stdin().read_line(&mut input).unwrap();
    let v:Vec<&str> = input.trim().split_whitespace().collect();
    let n = v[0];
    let t:u32 = v[1].parse().unwrap();
    let mut counter: u32 = 0;
    let mut index:u32 = 0;
    for c in n.chars().rev() {
        counter += convert_dec(c) as u32 * t.pow(index);
        index += 1;
    }
    println!("{}", counter)
}