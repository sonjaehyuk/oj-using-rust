use std::io::stdin;

struct Coin {
    quarter: i32,
    dime: i32,
    nickel: i32,
    penny: i32
}

impl Coin {
    fn new() -> Self {
        return Coin {
            quarter: 0,
            dime: 0,
            nickel: 0,
            penny: 0
        };
    }
}

fn main() {
    let mut input = String::new();
    stdin().read_line(&mut input).unwrap();
    let l: i32 = input.trim().parse().unwrap();

    let mut v:Vec<i32> = Vec::new();
    let mut coin_list:Vec<Coin> = Vec::new();
    for _ in 0..l {
        let mut input = String::new();
        stdin().read_line(&mut input).unwrap();
        let change: i32 = input.trim().parse().unwrap();
        v.push(change);
    }

    for value in &v {
        let mut value = value.clone();
        let mut coin = Coin::new();
        for fv in [25, 10, 5, 1] {
            let d = value / fv;
            match fv {
                25 => coin.quarter = d,
                10 => coin.dime = d,
                5 => coin.nickel = d,
                1 => coin.penny = d,
                _ => {}
            }
            value %= fv;
            if value == 0 {
                break
            }
        }
        coin_list.push(coin)
    }

    for coin in coin_list {
        println!("{} {} {} {}", coin.quarter, coin.dime, coin.nickel, coin.penny)
    }
}