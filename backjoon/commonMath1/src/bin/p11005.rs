use std::io;

fn main() {
    let mut input = String::new();
    io::stdin().read_line(&mut input).unwrap();
    let v: Vec<&str> = input.trim().split_whitespace().collect();

    let mut n: u32 = v[0].parse().unwrap();
    let t: u32 = v[1].parse().unwrap();
    let mut result = String::new();

    if t < 2 || t > 36 {
        return;
    }

    if n == 0 {
        println!("0");
        return;
    }

    while n > 0 {
        let value = n % t;
        n /= t;

        if value < 10 {
            result.push((value as u8 + b'0') as char);
        } else {
            result.push((value as u8 - 10 + b'A') as char);
        }
    }

    let converted: String = result.chars().rev().collect();
    println!("{}", converted);
}
