use std::io::stdin;

fn main() {
    // 가장 작은 정사각형의 개수는 작업을 1번 거칠 때마다 4^n(2^n * 2^n)으로 늘어난다.
    // 한 변당 점의 개수는 2^n + 1이다.
    let mut input = String::new();
    stdin().read_line(&mut input).unwrap();
    let n:u32 = input.trim().parse().unwrap();
    println!("{}", (2_u32.pow(n)+1).pow(2))
}