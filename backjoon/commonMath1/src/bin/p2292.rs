use std::io::stdin;

fn main() {
    let mut input = String::new();
    stdin().read_line(&mut input).unwrap();
    let n:i32 = input.trim().parse().unwrap();
    if n==1 {
        println!("1")
    } else {
        let mut m = 1;
        let mut adder = 1;
        loop {
            if 6* m <= n+4 && n+4 < 6*(m +adder) {
                println!("{}", adder+1);
                break;
            }

            m += adder;
            adder += 1;
        }
    }
}


#[test]
fn test_output() {
    use assert_cmd::Command;

    let mut cmd = Command::cargo_bin("p2292").unwrap();
    for i in 1..70 {
        let answer = match i {
            1 => 1,
            2..=7 => 2, // 6..12 6*1..6*2
            8..=19 => 3, // 12..24 6*2..6*4
            20..=37 => 4, // 24..42 6*4..6*7
            38..=61 => 5, // 42..66 6*7..6*11
            62..=70 => 6, // 66 6*11
            _ => panic!("overflow")
        };
        cmd.write_stdin(format!("{}\n", i)).assert().stdout(format!("{}\n", answer));
    }

}