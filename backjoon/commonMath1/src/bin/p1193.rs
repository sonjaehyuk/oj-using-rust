use std::io::stdin;

fn main() {
    let mut input = String::new();
    stdin().read_line(&mut input).unwrap();
    let mut n:i32 = input.trim().parse().unwrap();
    let mut line = 1;
    while n-line > 0 {
        n -= line;
        line += 1;
    }
    if line % 2 == 0 {
        println!("{}/{}", n, line+1-n);
    } else {
        println!("{}/{}", line+1-n, n);
    }
}

#[test]
fn test_1193() {
    use assert_cmd::Command;
    
    let mut cmd = Command::cargo_bin("p1193").unwrap();
    let v: Vec<(&str, &str)> = vec![("1", "1/1"), ("2", "1/2"), ("3", "2/1"),
                                    ("4", "3/1"), ("5", "2/2"), ("6", "1/3"),
                                    ("7", "1/4"), ("8", "2/3"), ("9", "3/2"), ("14", "2/4")];
    for i in v {
        cmd.write_stdin(format!("{}\n", i.0)).assert().stdout(format!("{}\n", i.1));
    }
}