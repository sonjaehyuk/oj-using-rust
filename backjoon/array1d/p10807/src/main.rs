use std::io;

fn main() {
    let mut array_size = String::new();
    io::stdin().read_line(&mut array_size).expect("입력 받기 실패");
    let array_size: i32 = array_size.trim().parse().expect("숫자 변환 실패");

    let mut list = String::new();
    io::stdin().read_line(&mut list).expect("입력 받기 실패");
    let list: Vec<i32> =  list.split_whitespace().map(|s| s.parse().expect("숫자 변환 실패")).collect();


    let mut find_number = String::new();
    io::stdin().read_line(&mut find_number).expect("입력 받기 실패");
    let find_number: i32 = find_number.trim().parse().expect("숫자 변환 실패");

    println!("{}", list.iter().filter(|&&x| x==find_number).count());
}
