use std::io::stdin;
use std::collections::HashSet;
fn main() {
    let mut counter: HashSet<i32> = HashSet::new();
    for _ in 0..10 {
        let mut input = String::new();
        stdin().read_line(&mut input).expect("입력 실패");
        let value: i32 = input.trim().parse().expect("변환 실패");

        let value = value % 42;
        counter.insert(value);
    }
    println!("{}", counter.len());
}
