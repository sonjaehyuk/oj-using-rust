fn main() {
    let mut input = String::new();
    std::io::stdin().read_line(&mut input).expect("입력 실패");
    let v:Vec<usize> = input.trim().split_whitespace().map(|s|s.parse().expect("숫자 변환 실패")).collect();
    let n = v[0] as i32;
    let mut basket: Vec<i32> = (1..(n+1)).collect();

    for _ in 0..v[1] {
        let mut input = String::new();
        std::io::stdin().read_line(&mut input).expect("입력 실패");
        let c:Vec<usize> = input.trim().split_whitespace().map(|s|s.parse().expect("숫자 변환 실패")).collect();
        let i = c[0] - 1;
        let j = c[1] - 1;

        let mut reverse_basket: Vec<i32> = Vec::new();

        for k in i..(j+1) {
            reverse_basket.push(basket[k]);
        }
        reverse_basket.reverse();
        basket.splice(i..(j+1), reverse_basket);
    }

    for i in 0..v[0] {
        print!("{} ", basket[i]);
    }

}
