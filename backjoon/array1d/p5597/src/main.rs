fn main() {
    let mut students:Vec<usize> = (1..31).collect();
    for _ in 0..28 {
        let mut input = String::new();
        std::io::stdin().read_line(&mut input).expect("입력 실패");
        let i: usize = input.trim().parse().expect("변환 실패");

        if let location = students.iter().position(|&x|x==i) {
            students.remove(location.unwrap());
        }
    }
    println!("{}\n{}", students[0], students[1]);
}
