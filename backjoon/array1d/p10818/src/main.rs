use std::io;

fn main() {
    let mut size = String::new();
    io::stdin().read_line(&mut size).expect("입력 받기 실패");
    let size: i32 = size.trim().parse().expect("숫자 변환 실패");

    let mut list = String::new();
    io::stdin().read_line(&mut list).expect("입력 받기 실패");
    let list: Vec<i32> = list.trim().split_whitespace().map(|s|s.parse().expect("숫자 변환 실패")).collect();

    let mut max: i32 = -1000000;
    let mut min: i32 = 1000000;
    if size == 1 {
        println!("{} {}", list[0], list[0]);
    }
    else {
        for i in list {
            if min >= i {
                min = i;
            }
            if max <= i {
                max = i;
            }
        }

        println!("{} {}", min, max);
    }

}