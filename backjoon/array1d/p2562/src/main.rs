use std::io;

fn main() {
    let size: i32 = 9;
    let mut max = 0;
    let mut index = 0;

    for i in 1..=size {
        let mut number = String::new();
        io::stdin().read_line(&mut number).expect("입력 실패");
        let number: i32 = number.trim().parse().expect("숫자 변환 실패");

        if number > max {
            max = number;
            index = i;
        }
    }

    println!("{}", max);
    println!("{}", index);
}
