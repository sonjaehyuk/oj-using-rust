use std::io;

fn main() {
    let mut score_count = String::new();
    io::stdin().read_line(&mut score_count).expect("입력 실패");
    let score_count: i32 = score_count.trim().parse().expect("숫자 변환 실패");

    let mut max = 0;
    let mut score = String::new();
    io::stdin().read_line(&mut score).expect("입력 실패");
    let score: Vec<i32> = score.trim().split_whitespace().map(|s| s.parse().expect("숫자 변환 실패")).collect();
    for i in &score {
        if *i > max {
           max = *i;
        }
    }

    let mut total:f32 = 0.0;
    for i in score {
        total = total + ((i as f32 / max as f32) * 100.0);
    }
    println!("{}", total/score_count as f32);
}
