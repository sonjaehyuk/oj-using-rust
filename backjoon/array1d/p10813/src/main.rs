fn main() {
    let mut input = String::new();
    std::io::stdin().read_line(&mut input).expect("입력 실패");
    let v:Vec<i32> = input.trim().split_whitespace().map(|s|s.parse().expect("숫자 변환 실패")).collect();

    let mut basket: Vec<i32> = (1..(v[0]+1)).collect();

    for _ in 0..v[1] {
        let mut input = String::new();
        std::io::stdin().read_line(&mut input).expect("입력 실패");
        let c:Vec<i32> = input.trim().split_whitespace().map(|s|s.parse().expect("숫자 변환 실패")).collect();

        let a = c[0] as usize - 1;
        let b = c[1] as usize - 1;
        let mut c: i32;

        c = basket[a];
        basket[a] = basket[b];
        basket[b] = c;
    }

    for i in 0..v[0] {
        print!("{} ", basket[i as usize]);
    }

}
