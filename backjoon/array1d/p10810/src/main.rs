fn main() {
    let mut input = String::new();
    std::io::stdin().read_line(&mut input).expect("입력 실패");
    let v:Vec<usize> = input.trim().split_whitespace().map(|s|s.parse().expect("숫자 변환 실패")).collect();

    let mut basket: Vec<i32> = vec![0; v[0]];

    for _ in 0..v[1] {
        let mut input = String::new();
        std::io::stdin().read_line(&mut input).expect("입력 실패");
        let c:Vec<i32> = input.trim().split_whitespace().map(|s|s.parse().expect("숫자 변환 실패")).collect();

        let start = c[0];
        let end = c[1];
        let value = c[2];

        for i in (start-1)..end {
            basket[i as usize] = value;
        }
    }

    for i in 0..v[0] {
        print!("{} ", basket[i]);
    }

}
