use std::io;

fn main() {
    let mut condition = String::new();
    io::stdin().read_line(&mut condition).expect("입력 실패");
    let condition: Vec<i32> = condition.trim().split_whitespace().map(|s|s.parse().expect("숫자 변환 실패")).collect();

    let mut list = String::new();
    io::stdin().read_line(&mut list).expect("입력 받기 실패");
    let list: Vec<i32> =  list.split_whitespace().map(|s| s.parse().expect("숫자 변환 실패")).collect();

    for i in list.iter() {
        if *i < condition[1] {
            print!("{} ", *i);
        }
    }
}