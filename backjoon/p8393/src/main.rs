use std::io::stdin;

fn main() {
    let mut input = String::new();
    stdin().read_line(&mut input).expect("");
    let value:i32 = input.trim().parse().expect("");
    let mut sum:i32 = 0;
    for i in 1..(value+1)
    {
        sum += i;
    }
    println!("{}", sum);
}
