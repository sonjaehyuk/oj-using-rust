use std::io::stdin;

fn main() {
    let mut input = String::new();

    stdin().read_line(&mut input).expect("");

    let parts: Vec<&str> = input.trim().split_whitespace().collect();

    let hour: i32 = parts[0].parse().expect("");
    let minute: i32 = parts[1].parse().expect("");

    if minute >= 45 {
        println!("{} {}", hour, minute-45);
    }
    else {
        if hour==0 {
            println!("{} {}", 23, minute+15);
        }
        else {
            println!("{} {}", hour-1, minute+15);
        }
    }
}
