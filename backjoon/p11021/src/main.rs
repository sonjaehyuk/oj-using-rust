use std::io::stdin;

fn main() {
    let mut input = String::new();
    stdin().read_line(&mut input).expect("");
    let value:i32 = input.trim().parse().expect("");

    let mut output:Vec<(i32, i32)> = Vec::new();
    for i in 0..value {
        let mut input = String::new();
        stdin().read_line(&mut input).expect("");

        let v:Vec<i32> = input.trim().split_whitespace().map(|x|x.parse().expect("")).collect();
        output.push((v[0]+v[1], i+1));
    }
    for j in output {
        println!("Case #{}: {}", j.1, j.0);
    }

}
