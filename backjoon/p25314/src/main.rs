use std::io::stdin;


fn main() {
    let mut input = String::new();
    stdin().read_line(&mut input).expect("");
    let byte_size:i32 = input.trim().parse().expect("");
    let mut i:i32 = 0;
    while i<byte_size {
        print!("long ");
        i += 4;
    }
    println!("int");

}
