use std::io::stdin;


fn main() {
    let mut input = String::new();
    stdin().read_line(&mut input).expect("");
    let t:i32 = input.trim().parse().expect("");
    let mut output:Vec<i32> = Vec::new();

    for _ in 0..t {
        let mut nums = String::new();
        stdin().read_line(&mut nums).expect("");
        let v: Vec<&str> = nums.trim().split_whitespace().collect();
        let one:i32 = v[0].parse().expect("");
        let two:i32 = v[1].parse().expect("");
        output.push(one+two);
    }
    for i in &output {
        println!("{}", i);
    }
}
