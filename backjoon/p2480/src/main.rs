use std::io::stdin;
use std::collections::HashSet;

fn main() {
    let mut input = String::new();

    stdin().read_line(&mut input).expect("");

    let parts: Vec<&str> = input.trim().split_whitespace().collect();

    let one: i32 = parts[0].parse().expect("");
    let two: i32 = parts[1].parse().expect("");
    let three: i32 = parts[2].parse().expect("");

    let mut numbers = HashSet::new();
    numbers.insert(one);
    numbers.insert(two);
    numbers.insert(three);

    if numbers.len() == 1 {
        println!("{}", 10000+one*1000);
    }
    else if numbers.len() == 2 {
        if one == two || one == three {
            println!("{}", 1000+one*100);
        }
        else {
            println!("{}", 1000+two*100);
        }
    }
    else {
        let mut max: i32 = 0;
        for part in numbers {
            if part > max {
                max = part;
            }
        }
        println!("{}", max*100);
    }

}
