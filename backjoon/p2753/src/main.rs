use std::io::stdin;

fn main() {
    let mut input = String::new();

    stdin().read_line(&mut input).expect("");

    let a: i32 = input.trim().parse().expect("숫자가 아님");

    if (a%4==0 && a%100!=0) || a%400==0 {
        println!("1");
    }
    else {
        println!("0");
    }
}
