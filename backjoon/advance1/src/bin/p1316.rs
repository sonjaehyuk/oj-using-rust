use std::io::stdin;

fn main() {
    let mut counter = 0;

    let mut input = String::new();
    stdin().read_line(&mut input).expect("입력 실패");
    let l:i32 = input.trim().parse().expect("변환 실패");
    counter = l;

    for _ in 0..l {
        let mut alphabet_flag:Vec<bool> = vec![false;26];
        let mut input = String::new();
        stdin().read_line(&mut input).expect("입력 실패");
        let text = input.trim().to_uppercase();
        let mut text = text.chars().peekable();

        while let Some(char) = text.next() {
            let i = (char as u8 - 65) as usize;
            if alphabet_flag[i] == true {
                counter -= 1;
                break;
            }
            alphabet_flag[i] = true; // 새 알파벳이면 등록
            // 다음까지 같은 알파벳이면 소진
            loop {
                match text.peek() {
                    Some(next_char) => {
                        if *next_char == char {
                            text.next();
                        } else {
                            break; // 다음 알파벳이 같지 않으면
                        }
                    },
                    None => {
                        break; // 다음 알파벳이 없으면
                    }
                }
            }
        }
    }
    println!("{}", counter);
}