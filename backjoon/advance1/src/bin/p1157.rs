use std::io::stdin;

fn main() {
    let mut count:Vec<i32> = vec![0;26];

    let mut input = String::new();
    stdin().read_line(&mut input).expect("입력 실패");
    let text = input.trim().to_uppercase();
    // 각 알파벳이 나온 횟수
    for char in text.chars() {
        let index: usize = (char as u8 - 65) as usize;
        count[index] += 1;
    }

    // 알파벳 등장 횟수 최댓값과 그 위치
    let mut max = -1;
    let mut max_index = 0;
    for c in 0..count.len() {
        let v = count[c];
        if v>max {
            max = v;
            max_index = c;
        }
    }
    // 알파벳 등장 최대 횟수가 여러 개인 경우
    let flag = count.iter().filter(|&&x|x==max).count();
    if flag > 1 {
        println!("?");
    } else {
        let max_char = char::from((max_index+65) as u8);
        println!("{}", max_char);
    }
}   