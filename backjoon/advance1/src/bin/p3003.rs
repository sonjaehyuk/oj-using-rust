use std::io::stdin;

fn main() {
    let set:Vec<i32> = vec![1, 1, 2, 2, 2, 8];

    let mut input = String::new();
    stdin().read_line(&mut input).expect("입력 실패");
    let user_input: Vec<i32> = input.trim().split_whitespace().map(|s|s.parse::<i32>().expect("변환 실패")).collect();

    let mut output: Vec<i32> = Vec::new();
    for i in 0..user_input.len() {
        let value = set[i] - user_input[i];
        output.push(value);
    }

    for i in output {
        print!("{} ", i);
    }
}