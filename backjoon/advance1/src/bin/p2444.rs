use std::io::stdin;

fn main() {
    let mut input = String::new();
    stdin().read_line(&mut input).expect("입력 실패");
    let t: i32 = input.trim().parse().expect("변환 실패");

    for i in 1..(t+1) {
        for _ in 0..(t-i) {
            print!(" ");
        }
        for _ in 0..(i*2-1) {
            print!("*");
        }
        print!("\n");
    }

    for i in (1..t).rev() {
        for _ in 0..(t-i) {
            print!(" ")
        }
        for _ in 0..(i*2-1) {
            print!("*");
        }
        print!("\n")
    }
}