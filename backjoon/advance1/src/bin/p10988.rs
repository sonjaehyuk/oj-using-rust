use std::io::stdin;

fn main() {
    let mut input = String::new();
    stdin().read_line(&mut input).expect("입력 실패");
    let text = input.trim().to_owned();
    let reverse_text = text.chars().rev().collect::<String>();

    let mut flag:bool = true;
    for i in 0..text.len() {
        if text.chars().nth(i) != reverse_text.chars().nth(i) {
            flag = false;
        }
    }
    if flag {
        println!("1");
    } else {
        println!("0");
    }

}