use std::io::stdin;

fn main() {
    let mut input = String::new();
    stdin().read_line(&mut input).expect("입력 실패");
    let text = input.trim();

    let mut text_counter = 0;
    let mut chars = text.chars().peekable();

    // 각 문자를 빼가는 방식으로
    while let Some(current_char) = chars.next() {
        match current_char {
            'c' => {
                if let Some('=') | Some('-') = chars.peek() {
                    chars.next(); // 뒤에 있는 = 또는 -를 pop
                }
            },
            'd' => {
                if let Some('z') = chars.peek() {
                    chars.next(); // 뒤에 있는 z를 pop
                    if let Some('=') = chars.peek() {
                        chars.next(); // 뒤에 있는 =를 pop
                    } else {
                        text_counter += 1; // 뒤에 있는 z pop
                    }
                } else if let Some('-') = chars.peek() {
                    chars.next(); // 뒤에 있는 -를 pop
                }
            },
            'l' | 'n' => {
                if let Some('j') = chars.peek() {
                    chars.next(); // 뒤에 있는 j를 pop
                }
            },
            's' | 'z' => {
                if let Some('=') = chars.peek() {
                    chars.next(); // 뒤에 있는 =를 pop
                }
            },
            _ => {}
        }
        text_counter += 1;
    }

    println!("{}", text_counter);
}
