use std::io::stdin;

fn main() {
    let mut total:f64 = 0.0;
    let mut counter:f64 = 0.0;
    for _ in 0..20 {
        let mut input = String::new();
        stdin().read_line(&mut input).expect("입력 실패");
        let v:Vec<&str> = input.trim().split_whitespace().collect();

        let time:f64 = v[1].parse().expect("변환 실패");
        match v[2] {
            "A+" => {
                total += 4.5*time;
                counter += time;
            },
            "A0" => {
                total += 4.0*time;
                counter += time;
            },
            "B+" => {
                total += 3.5*time;
                counter += time;
            },
            "B0" => {
                total += 3.0*time;
                counter += time;
            },
            "C+" => {
                total += 2.5*time;
                counter += time;
            },
            "C0" => {
                total += 2.0*time;
                counter += time;
            },
            "D+" => {
                total += 1.5*time;
                counter += time;
            },
            "D0" => {
                total += 1.0*time;
                counter += time;
            },
            "P" => {},
            "F" => {
                counter += time;
            },
            _ => {
            }
        }
    }
    let average:f64 = total / counter;
    println!("{}", average)
}