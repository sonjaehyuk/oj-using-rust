use std::io::stdin;

fn main() {
    loop {
        let mut input = String::new();
        stdin().read_line(&mut input).expect("입력 실패");
        let v:Vec<i32> = input.trim().split_whitespace().map(|x|x.parse().expect("i32 변환 실패")).collect();
        if v[0] == 0 && v[1] == 0 {
            break;
        }
        println!("{}", v[0] + v[1]);

    }

}
