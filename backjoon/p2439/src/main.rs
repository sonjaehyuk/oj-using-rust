use std::io::stdin;

fn main() {
    let mut input = String::new();
    stdin().read_line(&mut input).expect("입력 실패");
    let time: i32 = input.trim().parse().expect("i32로 변환 실패");
    for i in 1..time+1 {
        for _ in 1..time-i+1 {
            print!(" ");
        }
        for _ in 1..i+1 {
            print!("*");
        }
        println!();
    }
}
