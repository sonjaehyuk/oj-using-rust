use std::io::stdin;

fn main() {
    let mut v:Vec<char> = Vec::new();
    let mut counter:Vec<i32> = vec![-1;26];
    for i in 97..123 {
        v.push(char::from(i));
    }

    let mut input = String::new();
    stdin().read_line(&mut input).expect("입력 실패");

    let input = input.trim();
    for (c, i) in input.chars().zip(0..input.len()) {
        let location = v.iter().position(|&x|x==c).expect("검색 실패");

        match counter[location] {
            -1 => counter[location] = i as i32,
            _ => {

            }
        }
    }

    for c in counter {
        print!("{} ", c);
    }
}