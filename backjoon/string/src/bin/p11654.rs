use std::io::stdin;

fn main() {
    let mut input = String::new();
    stdin().read_line(&mut input).expect("입력 실패");
    let c: char = input.trim().parse().expect("변환 실패");
    println!("{}", c as u8);
}