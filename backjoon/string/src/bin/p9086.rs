use std::io::stdin;

struct Save {
    first: char,
    last: char
}

fn main() {
    let mut l:Vec<Save> = vec![];
    let mut input = String::new();
    stdin().read_line(&mut input).expect("입력 실패");
    let t: i32 = input.trim().parse().expect("변환 실패");
    for _ in 0..t {
        let mut input = String::new();
        stdin().read_line(&mut input).expect("입력 실패");
        let s = input.trim();
        let last_index = s.len();
        l.push(Save {
           first: char::from(s.as_bytes()[0]),
            last: char::from(s.as_bytes()[last_index-1]),
        });
    }

    for i in &l {
        println!("{}{}", i.first, i.last);
    }
}