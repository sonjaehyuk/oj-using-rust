use std::io::stdin;

fn main() {
    let mut input = String::new();
    stdin().read_line(&mut input).expect("입력 실패");
    let v:Vec<String> = input.trim().split_whitespace().map(|s|s.chars().rev().collect()).collect();
    let first_number:i32 = v[0].parse().expect("변환 실패");
    let second_number:i32 = v[1].parse().expect("변환 실패");
    if first_number > second_number {
        println!("{}", first_number);
    } else {
        println!("{}", second_number);
    }
}