use std::io::stdin;

fn main() {
    loop {
        let mut input = String::new();
        let status = stdin().read_line(&mut input).expect("입력 실패");
        if status>0 {
            println!("{}", input.trim());
        } else {
            break;
        }
    }
}