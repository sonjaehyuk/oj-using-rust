use std::io::stdin;

fn main() {
    let mut input = String::new();
    stdin().read_line(&mut input).expect("입력 실패");

    let input = input.trim();
    println!("{}", input.len());
}