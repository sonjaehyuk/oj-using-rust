use std::io::stdin;

fn main() {
    let mut input = String::new();
    stdin().read_line(&mut input).expect("입력 실패");
    let l:i32 = input.trim().parse().expect("변환 실패");
    let mut output:Vec<(i32, String)> = Vec::new();

    for _ in 0..l {

        let mut input = String::new();
        stdin().read_line(&mut input).expect("입력 실패");
        let v:Vec<&str> = input.trim().split_whitespace().collect();
        let r:i32 = v[0].parse().expect("변환 실패");
        let s = v[1].to_owned();
        output.push((r, s));


    }

    for (r, s) in output {
        for char in s.chars() {
            for _ in 0..r {
                print!("{}", char);
            }
        }
        print!("\n");
    }
}