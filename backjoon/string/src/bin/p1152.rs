use std::io::stdin;

fn main() {
    let mut input = String::new();
    stdin().read_line(&mut input).expect("입력 오류");
    let sentence: Vec<String> = input.trim().split_whitespace().map(|s|s.to_owned()).collect();
    println!("{}", sentence.len());
}