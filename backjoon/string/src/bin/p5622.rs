use std::io::stdin;

fn main() {
    let mut input = String::new();
    stdin().read_line(&mut input).expect("입력 실패");
    let input = input.trim();
    let mut total_count:i32 = 0;

    for char in input.chars() {
        match char {
            'A' | 'B' | 'C' => {
                total_count += 3
            },
            'D' | 'E' | 'F' => {
                total_count += 4
            },
            'G' | 'H' | 'I' => {
                total_count += 5
            },
            'J' | 'K' | 'L' => {
                total_count += 6
            },
            'M' | 'N' | 'O' => {
                total_count += 7
            },
            'P' | 'Q' | 'R' | 'S' => {
                total_count += 8
            }
            'T' | 'U' | 'V' => {
                total_count += 9
            },
            'W' | 'X' | 'Y' | 'Z' => {
                total_count += 10
            }
            _ => {

            }
        }
    }
    println!("{}", total_count);
}