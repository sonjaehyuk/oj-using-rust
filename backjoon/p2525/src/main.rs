use std::io::stdin;

fn main() {
    let mut input = String::new();
    stdin().read_line(&mut input).expect("");
    let parts: Vec<&str> = input.trim().split_whitespace().collect();
    let hour: i32 = parts[0].parse().expect("");
    let minute: i32 = parts[1].parse().expect("");
    let mut cook_time = String::new();
    stdin().read_line(&mut cook_time).expect("");
    let cook_time: i32 = cook_time.trim().parse().expect("");

    let hour = (hour+(minute+cook_time)/60)%24;
    let minute = (minute+cook_time)%60;
    println!("{} {}", hour, minute)
}