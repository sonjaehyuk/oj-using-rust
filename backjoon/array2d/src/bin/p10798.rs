use std::io::stdin;

fn main() {
    let mut v: Vec<String> = Vec::new();
    let mut result:Vec<char> = Vec::new();
    let mut max_len = 0;

    for _ in 0..5 {
        let mut input = String::new();
        stdin().read_line(&mut input).unwrap();
        let input: String = input.trim().to_owned();
        if input.len() > max_len {
            max_len = input.len();
        }
        v.push(input);
    }
    for i in 0..max_len {
        for j in 0..5 {
            if let Some(c) = v[j].chars().nth(i) {
                result.push(c);
            }
        }
    }
    result.iter().for_each(|x| { print!("{}", x) });
}