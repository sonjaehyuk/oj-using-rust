use std::io::stdin;

fn main() {
    let mut ground: Vec<Vec<bool>> = vec![vec![false; 100]; 100];
    let mut input = String::new();
    stdin().read_line(&mut input).unwrap();
    let count: usize = input.trim().parse().unwrap();
    for _ in 0..count {
        let mut input = String::new();
        stdin().read_line(&mut input).unwrap();
        let temp_v: Vec<usize> = input.trim().split_whitespace().map(|x1| {x1.parse().unwrap()}).collect();

        let x = temp_v[0];
        let y = temp_v[1];
        for r in x..(x+10) {
            ground[r][y..(y+10)].fill(true)
        }

    }

    let extent = ground.into_iter().flat_map(|x2: Vec<bool>| {x2}).filter(|&x3| {x3}).count();
    println!("{}", extent);
}