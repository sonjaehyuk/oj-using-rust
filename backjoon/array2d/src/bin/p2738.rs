use std::io::stdin;

fn main() {
    let mut a: Vec<Vec<i32>> = Vec::new();
    let mut b: Vec<Vec<i32>> = Vec::new();
    let mut c: Vec<Vec<i32>> = Vec::new();

    let mut input = String::new();
    stdin().read_line(&mut input).expect("입력 실패");
    let v:Vec<usize> = input.trim().split_whitespace().map(|x|x.parse::<usize>().expect("입력 실패")).collect();
    let n = v[0];
    let m = v[1];

    for _ in 0..n {
        let mut input = String::new();
        stdin().read_line(&mut input).expect("입력 실패");
        let temp_vec: Vec<i32> = input.trim().split_whitespace().map(|x|x.parse().unwrap()).collect();
        a.push(temp_vec);
        c.push(Vec::new());
    }
    for _ in 0..n {
        let mut input = String::new();
        stdin().read_line(&mut input).expect("입력 실패");
        let temp_vec: Vec<i32> = input.trim().split_whitespace().map(|x|x.parse().unwrap()).collect();
        b.push(temp_vec)
    }
    for i in 0..n {
        for j in 0..m {
            c[i].push(a[i][j] + b[i][j]);
        }
    }
    for i in 0..n {
        for j in 0..m {
            print!("{} ", c[i][j]);
        }
        println!();
    }


}