use std::io::stdin;

fn main() {
    let mut x: usize = 1;
    let mut y: usize = 1;
    let mut max: usize = 0;
    for i in 1..=9 {
        let mut input = String::new();
        stdin().read_line(&mut input).unwrap();
        let v: Vec<usize> = input.trim().split_whitespace().map(|x|x.parse().unwrap()).collect();
        for j in 1..=9 {
            if v[j-1] >= max {
                max = v[j-1];
                x = i;
                y = j;
            }
        }
    }
    println!("{}", max);
    println!("{} {}", x, y);
}