use std::io;

fn main() {
    let mut input = String::new();

    io::stdin().read_line(&mut input).expect("");

    let parts: Vec<&str> = input.trim().split_whitespace().collect();

    let a: i32 = parts[0].parse().expect("");
    let b: i32 = parts[1].parse().expect("");

    println!("{}", a + b);
}
