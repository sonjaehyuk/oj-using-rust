use std::io;

fn main() {
    let mut input = String::new();

    io::stdin().read_line(&mut input).expect("");

    let a: i32 = input.trim().parse().expect("숫자가 아님");

    if 90<=a && a<=100 {
        println!("A");
    }
    else if 80<=a && a<90 {
        println!("B");
    }
    else if 70<=a && a<80 {
        println!("C");
    }
    else if 60<=a && a<70 {
        println!("D");
    }
    else {
        println!("F");
    }
}
