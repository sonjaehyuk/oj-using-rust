use std::io::stdin;

fn main() {
    let mut input = String::new();
    stdin().read_line(&mut input).expect("");
    let value: i32 = input.trim().parse().expect("");
    for i in 1..10 {
        println!("{} * {} = {}", value, i, value*i);
    }

}
