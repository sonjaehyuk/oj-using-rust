use std::io::stdin;


fn main() {
    let mut input = String::new();
    stdin().read_line(&mut input).expect("");
    let paper_total:i32 = input.trim().parse().expect("");

    let mut input = String::new();
    stdin().read_line(&mut input).expect("");
    let t:i32 = input.trim().parse().expect("");

    let mut real_cost:i32 = 0;

    for _ in 0..t {
        let mut input = String::new();
        stdin().read_line(&mut input).expect("");
        let v:Vec<&str> = input.trim().split_whitespace().collect();
        real_cost += v[0].parse::<i32>().expect("") * v[1].parse::<i32>().expect("");
    }

    if real_cost == paper_total {
        println!("Yes");
    }
    else {
        println!("No");
    }

}
