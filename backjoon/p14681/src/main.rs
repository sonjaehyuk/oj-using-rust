use std::io::stdin;

fn main() {
    let mut x = String::new();
    let mut y = String::new();
    stdin().read_line(&mut x).expect("");
    stdin().read_line(&mut y).expect("");
    let x: i32 = x.trim().parse().expect("숫자 변환 불가능");
    let y: i32 = y.trim().parse().expect("숫자 변환 불가능");

    if x>0 && y>0 {
        println!("1");
    }
    else if x<0 && y>0 {
        println!("2");
    }
    else if x<0 && y<0 {
        println!("3");
    }
    else {
        println!("4");
    }
}
