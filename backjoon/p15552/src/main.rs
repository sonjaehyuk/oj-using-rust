use std::io::{stdin, stdout, BufRead, BufReader, Write, BufWriter};

fn main() {
    let mut lines = BufReader::new(stdin().lock()).lines();

    let t: i32 = lines.next().unwrap().unwrap().trim().parse().expect("");

    let mut output = Vec::new();

    for _ in 0..t {
        let nums = lines.next().unwrap().unwrap();
        let v: Vec<i32> = nums.trim().split_whitespace()
            .map(|x| x.parse().expect(""))
            .collect();
        output.push(v[0] + v[1]);
    }

    let mut handle = BufWriter::new(stdout().lock());
    for i in &output {
        writeln!(handle, "{}", i).unwrap();
    }
}
