use std::io::stdin;

fn main() {
    let stdin = stdin();
    loop {
        let mut input = String::new();
        let byte_read = stdin.read_line(&mut input).expect("입력 실패");
        if byte_read == 0 {
            break;
        }
        let v:Vec<i32> = input.trim().split_whitespace().map(|x|x.parse().expect("i32 변환 실패")).collect();
        println!("{}", v[0] + v[1]);

    }

}